Tech Stack:
Domain Language: Java
Domain Framework: Spring
Database Language: <TBD>


Chore App Idea:
I have chosen to build a website, designed for mobile though, that helps my older sister (she’s a mother of five: six, eight, 11, 11, and 12) manage the chores of the household by assigning them to a child to accomplish and then reward them with “money” in their own “Account.” My sister is constantly having to update her notepad iOS app with a current “Available Balance” and I think they don’t do enough chores around the house to help her out. I saw this as a way to help us both! My sister, and apparently many mothers (at least in the East Texas market), do not interact with a PC in their day-to-day lives. Also, my nieces and nephews have different layers of “rights” to their electronics; some have iPad’s, Amazon Fire Tablets, iPod’s (2) and a LG G5. The one thing they call can access is a browser on their home-WIFI network. My design for the app will mimic a downloadable app, with large fonts and centered event buttons, but will be usable on all standard devices.  